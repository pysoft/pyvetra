from matplotlib import pyplot as plt
import numpy as np
import glob as glob
import linecache
from cycler import cycler
from matplotlib import colors
import matplotlib.patches as mpatches

from plot import clb
from decoder import header
from utils import nametmp as nt
from analyse import analyse

class Plot(analyse.Analyse):
    def __init__(
        self
    ):
        """
        a class for plotting functions
        """

    def open_csv(self, vp, key, skiprows = 0, ravel = False, return_header = False, path = ''):  
        #print(path + '*' + vp + '*' + key + '*.csv')  
        path = glob.glob(path + '*' + vp + '*' + key + '*.csv')
        if len(path) != 0:
            path = path[0]
            data = np.loadtxt(path, skiprows = skiprows, delimiter = ',')
            if ravel == True:
                data = data.ravel()
            if return_header == True:
                try:
                    header = linecache.getline(path, 1)
                except:
                    print('Cannot read the header...')
                    return data, ''
                return data, header
            return data
        if return_header == True:
            return None, None    
        return None

    def mask_map(self, ax, vp, opt = '', path = ''):
        data = self.open_csv(vp, nt.name['mask'], skiprows = 0, path = path)
        nmasked = np.count_nonzero(data)
        textstr = ('masked = %.0f' % nmasked)
        ax.text(0.02, 1.02, textstr, transform = ax.transAxes, fontsize = 9)
        if data is not None:
            pcm = ax.pcolor(data[::-1], cmap = clb.mask(),linewidths = 1.)
            bounds = np.linspace(0,1,3)
            values = np.linspace(0,0.85,2)
            clba = plt.figure().colorbar(pcm, ticks = bounds, boundaries = bounds, values = values)
            pcm.set_clim(0,1)    
            clba.ax.set_title("Mask", fontsize = 11)
        if ax.has_data():
            ax.set_title(vp)
            ax.set_xlim([0, 256])
            ax.set_ylim([0, 256])
            ax.set_xlabel('Pixel col ID')
            ax.set_ylabel('Pixel row ID')
            ax.tick_params(axis = 'both', which = 'major', labelsize = 10)

    def trim_map(self, ax, vp, opt = '', path = ''):
        data = self.open_csv(vp, nt.name['trim'], skiprows = 0, path = path)
        if data is not None:
            pcm = ax.pcolor(data[::-1], cmap = clb.trim(),linewidths = 0.2)
            bounds = np.linspace(0, 15, 16)
            clba = plt.figure().colorbar(pcm, ticks=bounds, boundaries=bounds)
            pcm.set_clim(0,15)
            clba.ax.set_title("Trim [DAC]", fontsize = 11)
        if ax.has_data():
            ax.set_title(vp)
            ax.set_xlim(0, 256)
            ax.set_ylim(0, 256)
            ax.set_xlabel('Pixel row ID (0-255)', fontsize = 11)
            ax.set_ylabel('Pixel column ID (0-255)', fontsize = 11)
            ax.tick_params(axis='both', which='major', labelsize=10)

    def trim_hist(self, ax, vp, opt = '', path = ''):
        data = self.open_csv(vp, nt.name['trim'], skiprows = 0, ravel = True, path = path)

        mask = self.build_mask_pattern(ravel = True, build4 = True)
        bins = np.arange(-0.5, 16.5, 1)
        if data is not None:
            ax.hist(data[mask == 2], bins = bins, histtype = 'step', linewidth = 1.3,
                color = 'k', label = 'even col.', alpha = 0.9)
            ax.hist(data[mask == 1], bins = bins, histtype = 'step', linewidth = 1.3,
                color = 'r', label = 'odd col.', alpha = 0.9)
            ax.hist(data[mask == 4], bins = bins, histtype = 'step', linewidth = 1.3,
                color = 'k', label = 'even col. 16th rows', alpha = 0.9, ls = 'dashed')   
            ax.hist(data[mask == 3], bins = bins, histtype = 'step', linewidth = 1.3,
                color = 'r', label = 'odd col. 16th rows', alpha = 0.9, ls = 'dashed')   

        if ax.has_data():
            ax.set_title(vp)
            ax.set_xlim(-0.5, 15.5)
            ax.set_ylim(0, 7000)
            ax.set_axisbelow(True)
            ax.grid()
            ax.legend()
            ax.set_xlabel('Trim', fontsize = 11)
            ax.set_ylabel('Number of counts', fontsize = 11)
            ax.tick_params(axis='both', which='major', labelsize=10)

    def n_corr(self, ax, vp, opt = 'mean', path = ''):
        if opt == 'mean':
            self.n_scatter(ax, vp, ['n_mean_t0', 'n_width_t0'], path = path)
            self.n_scatter(ax, vp, ['n_mean_t15', 'n_width_t15'], path = path)
            self.n_scatter(ax, vp, ['n_mean_t16', 'n_width_t16'], path = path)
        elif opt == 'rate':
            self.n_scatter(ax, vp, ['n_rate_t0', 'n_width_t0'], path = path)
            self.n_scatter(ax, vp, ['n_rate_t15', 'n_width_t15'], path = path)
            self.n_scatter(ax, vp, ['n_rate_t16', 'n_width_t16'], path = path)

        if ax.has_data():
            ax.set_title(vp)
            ax.set_xlim(1300, 1900)
            ax.set_ylim(0, 40)
            ax.grid()
            k_patch = mpatches.Patch(color = 'gray', label = 'All, even col.')
            r_patch = mpatches.Patch(color = 'r', label = 'All, odd col.')
            b_patch = mpatches.Patch(color = 'b', label = '16th, even col.')
            ax.legend(handles = [k_patch, r_patch, b_patch], loc = 'upper right', fontsize = 10)
            ax.set_xlabel('DAC code', fontsize = 11)
            ax.set_ylabel('Sigma', fontsize = 11)
            ax.tick_params(axis='both', which='major', labelsize=10) 

    def n_scatter(self, ax, vp, opt, path = ''):
        if ('control' in path) and ('16' not in opt[0]):
            path = path.replace('control', 'normal')
        elif ('normal' in path) and ('16' in opt[0]):
            path = path.replace('normal', 'control')
        value = self.open_csv(vp, nt.name[opt[0]], skiprows = 1, ravel = True, path = path)
        sigma = self.open_csv(vp, nt.name[opt[1]], skiprows = 1, ravel = True, path = path)
        if value is None or sigma is None:
            return None

        cb = plt.cm.jet
        bin_x, bin_y = None, None
        trim = None
        if '0' in opt[1]:
            cb = ['gray', 'r', 'b']
        elif '15' in opt[1]:
            cb = ['gray', 'r', 'b']
        else:
            cb = ['gray', 'r', 'b']

        mask = self.build_mask_pattern(ravel = True)

        ax.scatter(value[mask == 2], sigma[mask == 2], c = cb[0], s = 0.002, alpha = 0.7)#bins = [bin_x, bin_y], norm = colors.LogNorm(), cmap = cb[0])
        ax.scatter(value[mask == 1], sigma[mask == 1], c = cb[1], s = 0.002, alpha = 0.7)#, bins = [bin_x, bin_y], norm = colors.LogNorm(), cmap = cb[1])
        ax.scatter(value[mask == 3], sigma[mask == 3], c = cb[2], s = 0.002, alpha = 0.7)


    def nw_map(self, ax, vp, opt = '', path = ''):
        data = self.open_csv(vp, nt.name['n_width_t' + opt], skiprows = 1, path = path)
        if data is not None:
            pcm = ax.pcolor(data[::-1], cmap = clb.noise(), linewidths = 2.0)
        if ax.has_data():
            clba = plt.figure().colorbar(pcm)
            clba.ax.set_title("Noise sigma [DAC]", fontsize = 11)
            pcm.set_clim(0,50)
            ax.set_title(vp)
            ax.set_xlim(0, 256)
            ax.set_ylim(0, 256)
            ax.set_xlabel('Pixel row ID (0-255)', fontsize = 11)
            ax.set_ylabel('Pixel column ID (0-255)', fontsize = 11)
            ax.tick_params(axis='both', which='major', labelsize=10)

    def nw_hist(self, ax, vp, opt = None, path = ''):
        task = 'n_width_t' + opt
        #c = [['blue', 'green']]
        bins = np.linspace(0,50,201)
        mask = self.build_mask_pattern(ravel = True, build4 = True)
        data = self.open_csv(vp, nt.name[task], skiprows = 1, ravel = True, path = path)
        #print(data)
        if data is not None:
            ax.hist(data[mask == 2], bins = bins, histtype = 'step', linewidth = 0.8, 
                    edgecolor = 'blue', label = 'All, even col.')#, ls = 'dotted')
            ax.hist(data[mask == 1], bins = bins, histtype = 'step', linewidth = 0.8, 
                    edgecolor = 'dodgerblue', label = 'All, odd col.')#, ls = 'dotted')
            ax.hist(data[mask == 4], bins = bins, histtype = 'step', linewidth = 0.8,
                    edgecolor = 'purple', label = '16ths, even col.')
            ax.hist(data[mask == 3], bins = bins, histtype = 'step', linewidth = 0.8,
                    edgecolor = 'darkblue', label = '16ths, odd col.')
            ax.hist(data, bins = bins, histtype = 'step', lw = 1.5,
                    edgecolor = 'red', label = 'All')#, alpha = 0.5)

        mean_alleven, std_alleven = self.mean_std(data[mask == 2], precision = 2, skip_zeros = True)
        mean_allodd, std_allodd = self.mean_std(data[mask == 1], precision = 2, skip_zeros = True)
        mean_16even, std_16even = self.mean_std(data[mask == 4], precision = 2, skip_zeros = True)
        mean_16odd, std_16odd = self.mean_std(data[mask == 3], precision = 2, skip_zeros = True)

        textstr = '\n'.join(('mean all even = %.2f'  % (mean_alleven, ),
                            'std. all even = %.2f'  % (std_alleven, ),
                            'mean all odd = %.2f'  % (mean_allodd, ),
                            'std. all odd = %.2f'  % (std_allodd, ),
                            'mean 16 even = %.2f'  % (mean_16even, ),
                            'std. 16 even = %.2f'  % (std_16even, ),
                            'mean 16 odd = %.2f'  % (mean_16odd, ),
                            'std. 16 odd = %.2f'    % (std_16odd,  )
        ))
        ax.text(0.02, 0.68, textstr, transform = ax.transAxes, fontsize = 9)

        if ax.has_data():
            ax.legend(fontsize = 9, loc = 'upper right')
            ax.set_title(vp)
            ax.set_xlim(0, 40)
            ax.grid()
            ax.set_xlabel('Noise width (sigma) [DAC]', fontsize = 11)
            ax.set_ylabel('Number of counts', fontsize = 11)
            ax.tick_params(axis='both', which='major', labelsize=10) 
            #ax.set_yscale('log', nonpositive = 'clip')      

    def n_map(self, ax, vp, opt = 'n_rate_t0', path = ''):
        data = self.open_csv(vp, nt.name[opt], skiprows = 1, path = path)
        if data is not None:
            pcm = ax.pcolor(data[::-1], cmap = 'jet', linewidths = 2.0)
        if ax.has_data():
            clba = plt.figure().colorbar(pcm)
            clba.ax.set_title("[DAC]", fontsize = 11)
            pcm.set_clim(1100,1900)
            ax.set_title(vp)
            ax.set_xlim(0, 256)
            ax.set_ylim(0, 256)
            ax.set_xlabel('Pixel column ID (0-255)', fontsize = 11)
            ax.set_ylabel('Pixel row ID (0-255)', fontsize = 11)
            ax.tick_params(axis='both', which='major', labelsize=10)

    def n_summary(self, ax, vp, opt = None, path = ''):
        scan_0 = self.open_csv(vp, nt.name['n_rate_t0'], skiprows = 1, ravel = True, path = path)
        scan_F = self.open_csv(vp, nt.name['n_rate_t15'], skiprows = 1, ravel = True, path = path)

        bins = np.linspace(1000,2000,201)
        bins_grand = np.linspace(1000,2000,501)
        if scan_0 is None or scan_F is None:
            return None

        ax.hist(scan_0[1::2], bins = bins, color = 'dodgerblue', alpha = 1,
                lw = 1, histtype = 'step', label = 't0, odd, bin_size=5')
        ax.hist(scan_0[::2], bins = bins, color = 'blue', alpha = 1, ls = 'dotted',
                lw = 1, histtype = 'step', label = 't0, even, bin_size=5')
        ax.hist(scan_0, bins = bins_grand, color = 'b', alpha = 0.2, 
                lw = 1, histtype = 'step', label = 't0, bin_size=2')
        ax.hist(scan_F[1::2], bins = bins, color = 'red', alpha = 1,
                lw = 1, histtype = 'step', label = 'tF, odd, bin_size=5')
        ax.hist(scan_F[::2], bins = bins, color = 'darkred', alpha = 1, ls = 'dotted',
                lw = 1, histtype = 'step', label = 'tF, odd, bin_size=5')
        ax.hist(scan_F, bins = bins_grand, color = 'r', alpha = 0.2, 
                lw = 1, histtype = 'step', label = 'tF, bin_size=2')

        mean_t0, std_t0 = self.mean_std(scan_0, precision = 2, skip_zeros = True)
        mean_tF, std_tF = self.mean_std(scan_F, precision = 2, skip_zeros = True)

        textstr = '\n'.join(('mean_t0 = %.2f'  % (mean_t0, ),
                            'std_t0 = %.2f'    % (std_t0,  ),
                            'mean_tF = %.2f'   % (mean_tF, ),
                            'std_tF = %.2f'    % (std_tF,  )
        ))
        ax.text(0.02, 0.85, textstr, transform = ax.transAxes, fontsize = 9)
        
        if ax.has_data():
            ax.legend(fontsize = 9, loc = 'upper right')
            ax.grid()
            ax.set_xlim([1200,2000])
            ax.set_ylim([1,65000])
            ax.set_title(vp)
            ax.set_xlabel('Threshold [DAC]')
            ax.set_ylabel('Number of pixels')
            ax.set_yscale('log', nonpositive = 'clip')

    def eq_summary(self, ax, vp, opt = None, path = ''):
        mask = self.open_csv(vp, nt.name['mask'], skiprows = 0, ravel = True, path = path)
        scan_control = self.open_csv(vp, nt.name['n_rate_t16'], skiprows = 1, ravel = True, path = path)
        baseline = self.open_csv(vp, nt.name['n_mean_t16'], skiprows = 1, ravel = True, path = path)

        if mask is None:
            mask = np.zeros([256,256])

        nmasked = np.sum(mask)
        mask_bool = (mask == 1)

        bin = np.linspace(1000,2000,501)
        mask = self.build_mask_pattern(ravel = True)
        try:
            #ax.hist(scan_control, bins = bin, color = 'red', alpha = 1,
            #    lw = 1, histtype = 'step', label = 'Total')
            ax.hist(baseline, bins = bin, color = 'k', alpha = 0.5, 
                lw = 1.2, histtype = 'step', label = 'Base, all')     
            ax.hist(scan_control[mask == 2], bins = bin, color = 'dimgray', alpha = 0.5,
                lw = 0.55, histtype = 'step', label = 'Rate, even')
            ax.hist(scan_control[mask == 1], bins = bin, color = 'k', alpha = 0.5, ls = 'dashed',
                lw = 0.55, histtype = 'step', label = 'Rate, odd')
            ax.hist(scan_control[mask == 3], bins = bin, color = 'darkblue', alpha = 0.5,
                lw = 0.55, histtype = 'step', label = 'Rate, 16th, odd')
            ax.hist(scan_control, bins = bin, color = 'r', alpha = 1,
                lw = 1.2, histtype = 'step', label = 'Rate, all')
        except:
            pass

        try:
            mean_bas = np.mean(baseline, where = baseline != 0)
        except:
            return None
        ax.axvline(mean_bas + 70, color = 'k', alpha = 0.65, ls = 'dashed', label = '70 DAC above baseline')

        ax.axvline(mean_bas - 70, color = 'gray', alpha = 0.65, ls = 'dashed', label = '70 DAC below baseline')
        mean_tc, std_tc = self.mean_std(scan_control, 2, skip_zeros = True)

        textstr = '\n'.join(('mean_tc = %.2f'   % (mean_tc, ),
                            'std_tc = %.2f'    % (std_tc, ),
                            'mask_over (temp.) = %.0f' % (std_tc * 5, ),
                            'threshold (temp.) = %.0f' % (mean_tc + std_tc * 6, ),
                            'masked = %.0f'    % (nmasked, )
        ))
        ax.text(0.02, 0.84, textstr, transform = ax.transAxes, fontsize = 8)

        if ax.has_data():
            ax.legend(fontsize = 8, loc = 'upper right')
            ax.grid()
            ax.set_xlim([1500,1850])
            ax.set_ylim([1,65000])
            ax.set_title(vp)
            ax.set_xlabel('Threshold [DAC]')
            ax.set_ylabel('Number of pixels')
            ax.set_yscale('log', nonpositive = 'clip')
    
    def dist_hist(self, ax, vp, opt = None, path = ''):
        scan_0 = self.open_csv(vp, nt.name['n_rate_t0'], skiprows = 1, path = path)
        scan_F = self.open_csv(vp, nt.name['n_rate_t15'], skiprows = 1, path = path)

        if (scan_0 is None) or (scan_F is None):
            return None

        dist = (scan_F - scan_0)
        dist = dist[dist != 0]

        bin_array = np.linspace(0,500,251)
        N, bins, patches = ax.hist(dist, bins = bin_array, color = 'blue', 
            label = "Distance between 0 & F", histtype='step', linewidth = 1.0)
        
        if ax.has_data():
            ax.legend()
            ax.grid()
            ax.set_xlim(0,500)
            ax.set_ylim(1,65000)
            ax.set_title(vp) 
            ax.set_xlabel('Distance [DAC]')
            ax.set_ylabel('Number of pixels')
            ax.set_yscale('log', nonpositive = 'clip')

        mean = round(np.nanmean(dist), 2)
        std = round(np.nanstd(dist), 2)
        mind, maxd = self.compute_boundary(dist, cond = 10, bin = bin_array)
        bin_array = np.linspace(1000,2000,201)
        min0, max0 = self.compute_boundary(scan_0, cond = 5, bin = bin_array)
        minF, maxF = self.compute_boundary(scan_F, cond = 5, bin = bin_array)
        
        textstr = '\n'.join(('mean = %.2f' % (mean, ),
                'std = %.2f'  % (std, ),
                'min = %.0f'  % (mind, ),
                'max = %.0f'  % (maxd, ),
                'min0 = %.0f' % (min0, ),
                'max0 = %.0f' % (max0, ),
                'minF = %.0f' % (minF, ),
                'maxF = %.0f' % (maxF, )
        ))
        ax.text(0.02, 0.69, textstr, transform = ax.transAxes, fontsize = 9)

    def pix_scan(self, ax, id, opt = None):
        try:
            self.get_pix_scan(ax, id, ['raw_t0', 'n_rate_t0'])
        except:
            pass
        try:
            self.get_pix_scan(ax, id, ['raw_t15', 'n_rate_t15'])
        except:
            pass
        try:
            self.get_pix_scan(ax, id, ['raw_t16', 'n_rate_t16'])
        except:
            pass
        if ax.has_data():
            ax.set_title(id)
            ax.set_xlim(1150,2000)
            ax.set_ylim(0, 80)
            ax.grid()
            ax.legend(fontsize = 10)
            ax.set_xlabel('DAC code', fontsize = 11)
            ax.set_ylabel('Counts', fontsize = 11)
            ax.tick_params(axis='both', which='major', labelsize=10) 

    def get_pix_scan(self, ax, id, opt):
        vp = id.split('_')[0]
        x, y = int(id.split('_')[1]), int(id.split('_')[2])
        data, head = self.open_csv(vp, nt.name[opt[0]], skiprows = 1, return_header = True)
        h = header.Header()
        h.read_to_bin(head)
        step, n_of_steps = h.step_size, h.n_of_steps
        min_thr, max_thr = h.min_thr, h.max_thr
        dp_y = np.zeros(n_of_steps)
        dp_x = np.arange(max_thr, min_thr, -1.*step)
        for k in range(n_of_steps):
            dp_y[k] = data[x + k*256][y]

        if '15' in opt[1]:
            clr = 'r'
        elif '0' in opt[1]:
            clr = 'b'
        else:
            clr = 'green'

        ax.plot(dp_x, dp_y, linewidth = 0.5, color = clr, marker = '.', markersize = 2, label = opt[1])
        noise_rate = self.open_csv(vp, nt.name[opt[1]], skiprows = 1)
        nrate = noise_rate[x][y]
        ax.axvline(x = nrate, lw = 1.5, c = 'k', ls = '--', alpha = 0.2, label = 'rate ' + str(nrate))
