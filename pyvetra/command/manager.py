#import numpy as np
import glob as glob
import os
from typing import Callable

from plot import plotcaller
from decoder import decode, header
from utils import asicinfo
from noise import noise
from monitoring import history
from status import status
from ivelo import ivelo
from recipe import recipehandler

class Run(plotcaller.PlotCaller, noise.Noise, history.History, status.Status):
    def __init__(
        self,
        task: dict = None
    ):
        """
        task list is built from client parser
        """
        self.task = task
    # equalisation functions

    def normal(self, id, mask = 1, path = '') -> None:
        data, header = decode.Decoder().read_and_decode(id, path = path, trim = '0')
        _, _, rate_0 = self.threshold_scan(data, header, path = path)

        data, header = decode.Decoder().read_and_decode(id, path = path, trim = '15')
        _, _, rate_F = self.threshold_scan(data, header, path = path)

        self.compute_trim(header.vp, rate_0, rate_F, path = path)

        f_normal = path + f'{header.vp}_normal_recipe.txt'
        open(f_normal, "w").close()

        self.find_range_control(rate_0, rate_F, f_normal)
    
    def quick(self, id, mask = 1, path = '') -> None:
        data, header = decode.Decoder().read_and_decode(id, path = path, trim = '0')
        header.max_thr += 64
        header.min_thr += 64
        mean_0, _, rate_0 = self.threshold_scan(data, header, path = path)

        data, header = decode.Decoder().read_and_decode(id, path = path, trim = '15')
        header.max_thr += 64
        header.min_thr += 64
        mean_F, _, rate_F = self.threshold_scan(data, header, path = path)

        self.compute_trim(header.vp, rate_0, rate_F, path = path)

        f_quick = path + f'{header.vp}_quick_recipe.txt'
        f_pixdac = path + f'{header.vp}_ipixeldac.txt'
        open(f_quick, "w").close()
        open(f_pixdac, "w").close()

        ipixeldac = self.find_ipixeldac(rate_0, rate_F, f_pixdac)

        self.find_range(mean_0, 0x0, f_quick, ipixeldac = ipixeldac)
        self.find_range(mean_F, 0xF, f_quick, ipixeldac = ipixeldac)

    def control(self, id, mask = 1, path = ''):

        #data, header, data_chunk = self.zeros(256, 256), None, None
        data, header = decode.Decoder().read_and_decode(id = id, path = path, trim = '16', suffix = str(1) + 'of' + str(mask))
        if header == None:
            print('... corrupted header, aborting')
            return None

        if header.module in [5,26]:
            if (header.vp == 'Module26_VP0-0' or header.vp == 'Module26_VP0-1' or header.vp == 'Module05_VP0-2'):
                print(header.vp, ' found, passing...')
                pass
            else:
                header.max_thr += 64
                header.min_thr += 64

        for i in range(mask - 1):
            data_chunk, _ = decode.Decoder().read_and_decode(id = id, path = path, trim = '16', suffix = str(i + 2) + 'of' + str(mask))
            #print(data.shape, data_chunk.shape)
            data[data_chunk != 0] = data_chunk[data_chunk != 0]

        d_mean, _, rate_c = self.threshold_scan(data, header, path = path)
        #cond is the DAC threshold above baseline
        self.generate_mask_recipe(d_mean, rate_c, header = header, cond = 60, path = path)
        self.generate_thr_recipe(d_mean, header = header, cond = 60, path = path)

    # search functions
    def find(self, key: str, f: Callable, cond: Callable, id = '', option = '') -> None:
        print('Searching for ' + str(cond) + ' in ' + key)
        f(key, id, cond)

    # monitoring functions
    def plot_mask(self, __, path = '') -> None:
        self.plot_cv('m_map', self.mask_map, path = path)

    def plot_trim(self, __, path = '') -> None: 
        self.plot_cv('t_hist', self.trim_hist, path = path)
        self.plot_cv('t_map', self.trim_map, path = path)

    def plot_normal(self, __, path = '') -> None:
        self.plot_cv('n_summary', self.n_summary, path = path)

    def plot_control(self, __, path = '') -> None:
        self.plot_cv('eq_summary', self.eq_summary, path = path)

    def plot_nc(self, __, path = '') -> None:
        self.plot_cv('nm_corr', self.n_corr, opt = 'mean', path = path)
        self.plot_cv('nr_corr', self.n_corr, opt = 'rate', path = path)

    def plot_ns(self, scan, path = '') -> None:
        if scan == 'normal' or scan == 'quick':
            self.plot_cv('ns_t0_map', self.nw_map, opt = '0', path = path)
            self.plot_cv('ns_t15_map', self.nw_map, opt = '15', path = path)
            self.plot_cv('ns_t0_hist', self.nw_hist, opt = '0', path = path)
            self.plot_cv('ns_t15_hist', self.nw_hist, opt = '15', path = path)
        elif scan == 'control':
            self.plot_cv('ns_t16_map', self.nw_map, opt = '16', path = path)
            self.plot_cv('ns_t16_hist', self.nw_hist, opt = '16', path = path)

    def plot_nr(self, scan, path = '') -> None:
        if scan == 'normal' or scan == 'quick':
            self.plot_cv('nr_t0_map', self.n_map, opt = 'n_rate_t0', path = path)
            self.plot_cv('nr_t15_map', self.n_map, opt = 'n_rate_t15', path = path)
        elif scan == 'control':
            self.plot_cv('nr_t16_map', self.n_map, opt = 'n_rate_t16', path = path)

    def plot_nm(self, scan, path = '') -> None:
        if scan == 'normal' or scan == 'quick':
            self.plot_cv('nm_t0_map', self.n_map, opt = 'n_mean_t0', path = path)
            self.plot_cv('nm_t15_map', self.n_map, opt = 'n_mean_t15', path = path)
        elif scan == 'control':
            self.plot_cv('nm_t16_map', self.n_map, opt = 'n_mean_t16', path = path)

    def plot_dist(self, __, path = '') -> None:
        self.plot_cv('dist_hist', self.dist_hist, path = path)

    def plot_pix(self) -> None:
        pix = self.task['pix'][0] + '_' + self.task['pix'][1] + '_' + self.task['pix'][2]
        self.plot('pixel_' + pix, self.pix_scan, id = pix, path = path)
    
    def plot_all(self, scan, path = '') -> None:
        if scan == 'normal' or scan == 'quick':
            self.plot_trim(scan, path = path)
            self.plot_normal(scan, path = path)
            self.plot_nc(scan, path = path)
            self.plot_ns(scan, path = path)
            self.plot_nr(scan, path = path)
            self.plot_nm(scan, path = path)
            self.plot_dist(scan, path = path)
        if scan == 'control':
            self.plot_mask(scan, path = path)
            self.plot_control(scan, path = path)
            self.plot_ns(scan, path = path)
            self.plot_nr(scan, path = path)

    # main manager
    def parse(self) -> None:
        if self.task["which"] == "equalisation":
            if self.task['velo']:
                for mod in range(52):
                    path = 'Module' + str(mod).zfill(2) + '/' + self.task['scan_type'] + '/'
                    if os.path.exists(path):
                        for id in asicinfo.asic_list:
                            try:
                                getattr(self, self.task['scan_type'])(id, int(self.task['masked_scans']), path)
                            except:
                                pass
            else:    
                id = self.task['velopix']
                if id == 'all':
                    for id in asicinfo.asic_list:
                        getattr(self, self.task['scan_type'])(id, int(self.task['masked_scans']))
                else:
                    getattr(self, self.task['scan_type'])(id, int(self.task['masked_scans']))
        
        if self.task['which'] == 'noise':
            if self.task['analysis']:
                #self.noise_study(self.task['analysis'])
                #self.spread_study(self.task['analysis'])
                #self.map_baseline(self.task['analysis'])
                self.nsplit_study(self.task['analysis'])

        if self.task['which'] == 'search':
            key = self.task['key']
            cond = lambda a: eval(self.task['criterion'])
            id = self.task['velopix']
            self.find(key, self.find_pixel, cond = cond, id = id)

        # plot manager
        if self.task['which'] == 'plot':
            if self.task['velo'] is None: 
                if self.task['all'] is None: 
                    for element in self.task:
                        if self.task[element] != None and element != 'which' and element != 'scan_type':
                            getattr(self, element)(self.task['scan_type'])
                elif self.task['all'] is True:
                    self.plot_all(self.task['scan_type'])

            elif self.task['velo'] is True:
                for mod in range(52):
                    path = 'Module' + str(mod).zfill(2) + '/' + self.task['scan_type'] + '/'
                    if os.path.exists(path):
                        if not os.path.exists(path + 'plot') and self.task["which"] == "plot":
                            os.makedirs(path + 'plot')
                        #for id in asicinfo.asic_list:
                        if self.task['all'] is None:
                            for element in self.task:
                                if self.task[element] != None and element != 'which' and element != 'scan_type' and element != 'velo':
                                    getattr(self, element)(self.task['scan_type'], path = path)
                        elif self.task['all'] is True:
                            self.plot_all(self.task['scan_type'], path = path)

        if self.task["which"] == "monitoring":
            if self.task['history']:
                self.group_history(25)

        if self.task['which'] == 'status':
            if self.task['status_of'] == 'equalisation':
                self.status_manager(self.task['dir'])
            if self.task['status_of'] == 'update':
                #self.make_overview(self.task['dir'], which = 'quick')
                #self.make_overview(self.task['dir'], which = 'normal')
                self.make_overview(self.task['dir'], which = 'control')
            if self.task['status_of'] == 'masked': 
                self.status_masked(self.task['dir'])
            if self.task['status_of'] == 'spread':
                self.status_spread(self.task['dir'])
            if self.task['status_of'] == 'threshold':
                self.status_threshold(self.task['dir'])
            if self.task['status_of'] == 'baseline':
                self.status_mean(self.task['dir'])
                #self.status_mean(self.task['dir'])
                #self.status_if_actual(self.task['dir'])
            if self.task['status_of'] == 'dacsettings':
                #self.status_dacsettings(self.task['dir'], self.task['tag']) 
                self.correlation_dacsettings_noise(self.task['dir'], self.task['tag'])
            if self.task['status_of'] == 'summary':
                self.make_calib_summary(self.task['dir'])

        if self.task['which'] == 'recipe':
            if self.task['recipe_of'] == 'threshold':
                rc = recipehandler.RecipeHandler()
                rc.build_thr_recipe(self.task['dir'])

        if self.task['which'] == 'ivelo':
            gi = ivelo.Ivelo()
            gi.run(self.task['dir'])

