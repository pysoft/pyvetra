import time
import numpy as np
import glob as glob
import matplotlib.pyplot as plt

from utils import nametmp as nt

class Analyse():
    def __init__(
        self
    ):
        """
        """

    def threshold_scan(self, data, header, save = 1, slope = 'right', path = ''):
        print("Processing a threshold scan...")

        start = time.time()
        thr_range = np.arange(header.max_thr , header.min_thr , -header.step_size)
        data = data.astype(dtype = np.uint32).reshape((-1,256,256))
        d_mean, d_std = self.threshold_mean_std(data, thr_range)

        end = time.time()
        #print("...time for computing mean & std is ", end - start)

        if save == 1:
            start = time.time()
            np.savetxt(path + header.vp + '_scan' + str(header.trim) + '_mean.csv', d_mean, delimiter = ',',
                    header = header.readable, fmt = '%.2f')
            np.savetxt(path + header.vp + '_scan' + str(header.trim) + '_std.csv', d_std, delimiter = ',', 
                    header = header.readable, fmt = '%.2f')
            end = time.time()
            #print("...saving mean & std, time elapsed: ", end - start)

        #start = time.time()
        rate = self.threshold_rate(data, header.min_thr, header.max_thr, header.step_size, slope = slope)

        if save == 1:
            np.savetxt(path + header.vp + '_scan' + str(header.trim) + '_rate.csv', rate, delimiter = ',',
                    header = header.readable, fmt = '%.u')
        #end = time.time()
        #print("...computing noise rate, time elapsed: ", end - start)
        return d_mean, d_std, rate

    def generate_mask_recipe(self, dmean, drate, header, cond, path = ''):
        print(f"Generating mask for {cond} DAC threshold ...")
        maskmap = np.zeros([256,256])
        mdmean = np.mean(dmean, where = dmean != 0)
        maskmap[(mdmean + cond - drate) <= 0] = 1
        print(f'... masked {np.sum(maskmap)} pixels') 
        np.savetxt(path + header.vp + '_mask_recipe.csv', maskmap, delimiter = ',', fmt = '%.u')
        return maskmap

    def generate_thr_recipe(self, dmean, header, cond, path = '') -> int:
        print(f"Generating threshold for {cond} DAC threshold ...")
        mdmean = np.mean(dmean, where = dmean != 0)
        thr = np.array([mdmean + cond])
        print(f'... computed threshold at {thr} DAC') 
        np.savetxt(path + header.vp + '_threshold_recipe.csv', thr, fmt = '%.u')
        return thr

    def build_mask_pattern(self, ravel = False, build4 = False) -> np.ndarray:
        mask = np.ones([256,256])
        mask[:,::2] = 2
        mask[15::16, 1::2] = 3
        if build4 == True:
            mask[15::16, ::2] = 4
        if ravel == True:
            return mask.ravel()
        else:
            return mask

    def zeros(self, a, b):
        return np.zeros([a, b])

    def compute_trim_old(self, vp, rate_0, rate_F, save = 1):
        print("\nEqualising...")
        mean_0, mean_F = np.mean(rate_0), np.mean(rate_F)

        target = 1/2 * (mean_0 + mean_F)
        #max_trim = 15

        trim = np.zeros([256, 256])
        #print("Zero elements: ", np.count_nonzero(mean_0 == 0), np.count_nonzero(mean_F == 0))
        #counters = 0
        trim_scale = 1.*(rate_F - rate_0) / 15
        #print("Trim scale lower than 0: ", np.count_nonzero(trim_scale <= 0))
        #print(trim, trim_scale, target_even, target_odd, target_16th)
        if trim_scale is not None:
            trim = np.round(np.divide((target - rate_0),
                trim_scale, where = trim_scale != 0))            
            trim[trim > 15] = 15
            trim[trim < 0 ] = 0

        if save == 1:
            start = time.time()
            np.savetxt(path + vp + '_trim_recipe.csv', trim, delimiter = ',', fmt = '%.u')
            #np.savetxt(path + vp + '_mask_recipe.csv', np.zeros([256,256]), delimiter = ',', fmt = '%.u')
            end = time.time()
            print("...saving recipe, time elapsed: ", end - start)

    def compute_trim(self, vp, rate_0, rate_F, save = 1, path = ''):
        print("\nEqualising...")
        #mean_0, mean_F = np.mean(rate_0), np.mean(rate_F)

        mask = self.build_mask_pattern()

        target_even = 1/2 * (np.mean(rate_0[mask == 2]) + np.mean(rate_F[mask == 2]))
        target_odd = 1/2 * (np.mean(rate_0[mask == 1]) + np.mean(rate_F[mask == 1]))
        target_16th = 1/2 * (np.mean(rate_0[mask == 3]) + np.mean(rate_F[mask == 3]))
        target_16th = target_odd - 5
        #target = 1/2 * (mean_0 + mean_F)
        #max_trim = 15

        trim = np.zeros([256, 256])

        #print("Zero elements: ", np.count_nonzero(mean_0 == 0), np.count_nonzero(mean_F == 0))
        #counters = 0
        trim_scale = 1.*(rate_F - rate_0) / 15
        #print("Trim scale lower than 0: ", np.count_nonzero(trim_scale <= 0))
        if trim_scale is not None:
            trim[mask == 2] = np.round(np.divide((target_even - rate_0[mask == 2]),
                trim_scale[mask == 2], where = trim_scale[mask == 2] != 0))
            trim[mask == 1] = np.round(np.divide((target_odd - rate_0[mask == 1]),
                trim_scale[mask == 1], where = trim_scale[mask == 1] != 0))
            trim[mask == 3] = np.round(np.divide((target_16th - rate_0[mask == 3]),
                trim_scale[mask == 3], where = trim_scale[mask == 3] != 0))                
            trim[trim > 15] = 15
            trim[trim < 0 ] = 0

        if save == 1:
            start = time.time()
            np.savetxt(path + vp + '_trim_recipe.csv', trim, delimiter = ',', fmt = '%.u')
            #np.savetxt(path + vp + '_mask_recipe.csv', np.zeros([256,256]), delimiter = ',', fmt = '%.u')
            end = time.time()
            print("...saving recipe, time elapsed: ", end - start)

    def threshold_rate(self, data, min_thr, max_thr, step_size, slope = 'right'):
        if slope == 'left':
            data = np.flip(data, axis = 0)
        satmax = np.argmax(data > 31.5, axis = 0)
        #satmax[satmax == 0] = np.nan
        slopemax_l = np.float32(data[satmax, np.arange(256)[:, np.newaxis], np.arange(256)])
        slopemax_h = np.float32(data[satmax - 1, np.arange(256)[:, np.newaxis], np.arange(256)])
        grad = (slopemax_h - slopemax_l)/(step_size)
        if slope == 'right':
            dacmax = (max_thr - satmax * step_size)
        elif slope == 'left':
            dacmax = (min_thr + satmax * step_size)
        #satmax_dac[satmax_dac == 1950] = 0
        grad[grad == 0.0] = float('inf')
        slopemid = np.round(dacmax - (slopemax_l - 31.5)/grad)
        return slopemid

    def threshold_mean_std(self, data, thr_range):
        if data.dtype != np.uint32:
            data = data.astype(dtype = np.uint32)

        d_total = np.sum(data, axis = 0, dtype = np.float32)
        d_total[d_total == 0] = np.nan
        d_times_thr = data * thr_range[:, np.newaxis, np.newaxis]
        d_integral = np.sum(d_times_thr, axis = 0)
        d_mean = np.divide(d_integral, d_total)#, where = (d_total != 0))

        m_squared = np.array([np.full((256,256), thr) - d_mean for thr in thr_range]) ** 2
        d_times_squared = data * m_squared 
        d_integral = np.sum(d_times_squared, axis = 0)
        d_sigma = np.sqrt(d_integral/d_total)

        d_mean = np.nan_to_num(d_mean, nan = 0)#.astype(np.uint32)
        d_sigma = np.nan_to_num(d_sigma, nan = 0)

        return d_mean, d_sigma


    def threshold_std(self, data, thr_range):
        if data.dtype != np.uint32:
            data = data.astype(dtype = np.uint32)
        m_squared = np.array([np.full((256,256), thr) - d_mean for thr in thr_range]) ** 2
        d_times_squared = data * m_squared 
        d_integral = np.sum(d_times_squared, axis = 0)
        d_sigma = np.sqrt(d_integral / d_total)

    def compute_boundary(self, dist, cond, bin):
        N, bins = np.histogram(dist, bins = bin)
        min_u, max_u = None, None
        for it in range(len(N)-1):
            if N[it] > cond:
                max_u = bins[it]
            if N[-1-it] > cond:
                min_u = bins[-1-it]
        if min_u is not None and max_u is not None:
            return min_u, max_u    
        else:
            return np.nan, np.nan

    def mean_std(self, scan, precision = 2, skip_zeros = False):
        try:
            if skip_zeros == False:
                scan_mean = round(np.nanmean(scan), precision)
                scan_std = round(np.nanstd(scan), precision)
            else:
                scan_mean = round(np.nanmean(scan[scan!=0]), precision)
                scan_std = round(np.nanstd(scan[scan!=0]), precision)                
        except:
            scan_mean, scan_std = np.nan, np.nan
        return scan_mean, scan_std

    def find_pixel(self, key, vp, cond):
        total = 0
        try:
            data = self.open_csv(vp, nt.name[key], skiprows = 0)
        except:
            data = self.open_csv(vp, nt.name[key], skiprows = 1)
        for i in range(256):
            for j in range(256):
                if cond(data[i,j]):
                    print('X: ', i, ' Y: ', j )
                    total += 1
        print("Found " + str(total) + ' pixels meeting criteria')

    def analyse_distance(self, mfile, vp, date, opt):
        if vp == 'VP0-0':
            mfile.write('VP,mean,std,min,max,min0,max0,minF,maxF,mask\n')
        mask = self.open_csv(vp, nt.name[opt[0]], date, skiprows = 0)
        scan_0 = self.open_csv(vp, nt.name[opt[1]], date, skiprows = 1)
        scan_F = self.open_csv(vp, nt.name[opt[2]], date, skiprows = 1)

        if (scan_0 is None) or (scan_F is None):
            return None

        dist = abs((scan_F - scan_0).ravel())

        if (mask is not None):
            masked = np.sum(mask)
            for pix in range(256* 256):
                if mask[pix] == 1:
                    dist[pix] = np.nan

        bin_array = np.linspace(0,500,251)

        N, bins = np.histogram(dist, bins = bin_array)
                
        mean = round(np.nanmean(dist), 2)
        std = round(np.nanstd(dist), 2)
        mind, maxd = self.compute_boundary(dist, cond = 10, bin = bin_array)
        bin_array = np.linspace(1000,2000,201)
        min0, max0 = self.compute_boundary(scan_0, cond = 5, bin = bin_array)
        minF, maxF = self.compute_boundary(scan_F, cond = 5, bin = bin_array)

        mfile.write(vp)
        mfile.write(',' + str(mean) + ',' + str(std) + ',' + str(mind) + 
                    ',' + str(maxd) + ',' + str(min0) + ',' + str(max0) + ','
                    + str(minF) + ',' + str(maxF) + ',' + str(masked) + '\n')

    def find_ipixeldac(self, rate_0, rate_F, fn):
        dist = abs((rate_F - rate_0))
        dist_mean = np.mean(dist, where = dist > 0)
        aim_interval = 185
        ipixeldac_default = 90
        tr_factor = 2.49
        dac_to_add = ((aim_interval - dist_mean)/tr_factor)

        ipixeldac = int(ipixeldac_default + dac_to_add)
        with open(fn, "a") as file:
            file.write(f"{ipixeldac}")
        print("Compute ipixeldac to ", ipixeldac)
        return ipixeldac
    
    def find_range(self, mean, trim, fn, ipixeldac = None):
        if not mean.any():
            return None

        low_thr = np.mean(mean, where = mean != 0) - 7.5*np.std(mean, where = mean != 0) 
        high_thr = np.mean(mean, where = mean != 0) + 7.5*np.std(mean, where = mean != 0)
        if trim == 0x0:
            low_thr -= (ipixeldac-90)*2
            high_thr -= (ipixeldac-90)
        elif trim == 0xF:
            low_thr += (ipixeldac-90)
            high_thr += (ipixeldac-90)*2
        if high_thr > 1980: high_thr = 1980

        low_thr = int(np.round(low_thr/5)*5)
        high_thr = int(np.round(high_thr/5)*5) 

        with open(fn, "a") as file:
            file.write(f"trim{trim}low_{low_thr}\n")
            file.write(f"trim{trim}hgh_{high_thr}\n")
        
        print("Compute range ", low_thr, high_thr)

    def find_range_control(self, rate_0, rate_F, fn):
        if not rate_0.any():
            return None

        #2.5 is a conversion between width and sigma when computed on width of 2 step sizes, 35 is ~max. noise expected
        low_thr = int(np.mean(rate_F, where = rate_F != 0) - 4.5*np.std(rate_F, where = rate_F != 0) - 2.5 * 35)
        high_thr = int(np.mean(rate_0, where = rate_0 != 0) + 6*np.std(rate_0, where = rate_0 != 0))

        with open(fn, "a") as file:
            file.write(f"low_{low_thr}\n")
            file.write(f"high_{high_thr}")
        print("Compute range ", low_thr, high_thr)

