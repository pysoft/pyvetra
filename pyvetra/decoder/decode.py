import numpy as np
import glob as glob
import matplotlib.pyplot as plt
import os

import time

from decoder import header
from utils.lfsr import lfsr_to_hit as lfsr
from utils.hash_table import hash_table as hash_table

class Decoder():
    def __init__(
        self
    ):
        """
        """

    def read_and_decode(self, id, trim, suffix = '1of1', path = '', save = 0):
        print("Decoding...")
        print(path + '*' + id + '*scan' + trim + '_*' + suffix + '.dat')
        fsize = 0
        try:
            fpath = glob.glob(path + '*' + id + '*scan' + trim + '_*' + suffix + '.dat')[0]
            f = open(fpath, 'rb')
            fsize = os.path.getsize(fpath)
        except:
            print("...cannot open the file! terminating...")
            return None, None
        head = header.Header(f.read(28))
        print("...found the following header: " + head.readable)
        if not head.is_valid(fsize):
            return None, None
        start = time.time()
        data = self.decode(f, head.n_of_steps)
        end = time.time()
        print("...time elapsed: ", np.round(end - start, 2), 's')
        if save == 1:
            start = time.time()
            np.savetxt(path + head.vp + '_scan' + trim + '_raw' + '_' + suffix + '.csv', data, delimiter = ',', 
                       header = head.readable, fmt = '%.u')
            end = time.time()
            print("...saving decoded raw data, time elapsed: ", end - start)
        return data, head

    def decode(self, f, nsteps):
        matrix = np.zeros([256*nsteps, 256], dtype = np.uint8)
        mtemp = np.zeros([256,256], dtype = np.uint8)
        for step in range(nsteps):
            for i in range(256):
                for j in range(64):
                    frame = f.read(3)
                    if frame == b'\x00\x00\x00':
                        mtemp[4*j : 4*j + 4, i] = 0
                    elif frame in hash_table:
                        mtemp[4*j : 4*j + 4, i] = hash_table[frame]
                    else:
                        val_lfsr = (frame[0] & 0xFC) >> 2
                        mtemp[4*j + 0, i] = lfsr[val_lfsr]

                        val_lfsr = (frame[1] & 0xF0) >> 4 | (frame[0] & 0x03) << 4
                        mtemp[4*j + 1, i] = lfsr[val_lfsr]                

                        val_lfsr = (frame[2] & 0xC0) >> 6 | (frame[1] & 0x0F) << 2
                        mtemp[4*j + 2, i] = lfsr[val_lfsr]

                        val_lfsr = (frame[2] & 0x3F)
                        mtemp[4*j + 3, i] = lfsr[val_lfsr]
            matrix[256*step : 256*(step+1)] = mtemp
        return matrix


