import numpy as np
import glob as glob
import matplotlib.pyplot as plt
import os
import logging
import time
import pandas as pd

from decoder import header
from utils import asicinfo, functions
#from utils.lfsr import lfsr_to_hit as lfsr
#from utils.hash_table import hash_table as hash_table

class Recipe_handler():
    def __init__(
        self
    ):
        """
        """
        #for element in list_settings:
        #    print(element)
        #    for line in file:
        #        line = line.strip()
        #        print(line)
        #        if element in line:
        #            key,value = line.split('=')
        #            list_values.append(value)
        #            print(element, value)
        #            break    

        # 
    def read_recipe(self, file, list_settings):
        list_values = []
        line_list = []
        for line in file:         
            line_list.append(line.strip())

        for element in list_settings:   
            for line in line_list:
                    if element in line:
                        (key,value) = line.split('=')
                        list_values.append(value)

        return list_values

    def recipe_to_df(self, recipe_path, recipe_tag):

        csv_present = any(filename.endswith('.csv') for filename in os.listdir(recipe_path))
        if csv_present:
            print('Reading existing recipe csv file...')
            df = pd.read_csv(f'{recipe_path}/recipe{recipe_tag}.csv')
        else:
            print(f'Building csv file from recipe {recipe_tag}...')
            list_settings = asicinfo.dac_settings + ['threshold']
            list_values = []
            df = functions.build_velo_df(list_settings)
            index = 0
            for mod in range(52):           
                mod = str(mod).zfill(2)
                for asic in asicinfo.asic_list:
                    try:
                        with open(f'{recipe_path}/Module{mod}/Module{mod}_{asic}.dat', 'r') as f:
                            list_values= self.read_recipe(f, list_settings)                  
                    except Exception as et: logging.exception(f'Error occurred {et}')
                    df.loc[index, list_settings] = list_values
                    index += 1
            df.to_csv(f'recipe{recipe_tag}.csv')       
        return df

    

    

            