# temporary util file until filenames are defined 
name = {
    ""               : "",
    "mask"           : "mask_recipe",
    "trim"           : "trim_recipe",
    "n_mean_t0"      : "scan0_mean",
    "n_mean_t15"     : "scan15_mean",
    "n_mean_t16"     : "scan16_mean",
    "n_rate_t0"      : "scan0_rate",
    "n_rate_t15"     : "scan15_rate",
    "n_rate_t16"     : "scan16_rate",
    "n_rate_predict" : "predict_rate",
    "n_width_t0"     : "scan0_std",
    "n_width_t15"    : "scan15_std",
    "n_width_t16"    : "scan16_std",
    "raw_t0"         : "scan0_raw",
    "raw_t15"        : "scan15_raw",
    "raw_t16"        : "scan16_raw"
    }
