import argparse

parser = argparse.ArgumentParser()
subparser = parser.add_subparsers(help = 'Available options')

# a scheme for a parser for further development
dummy_parser = subparser.add_parser('...', help = '...')
dummy_parser.add_argument('dummy', action = None, choices = None, default = None,
                         nargs = None, metavar = None, help = None)

# equalisation parser
equalisation_parser = subparser.add_parser('equalisation', help = 'handle a quick/normal/control scan')
equalisation_parser.set_defaults(which = 'equalisation')
equalisation_parser.add_argument('scan_type', action = 'store', choices = ['quick', 'normal', 'control'],
                                help = 'select the type of the scan to analyse')
equalisation_parser.add_argument('--velopix', '-vp', action = 'store', const = 'all', default = 'all', nargs = '?',
                                help = 'select the ASIC, default - all ASICs in the module')
equalisation_parser.add_argument('--masked-scans', '-ms', action = 'store', const = 1, default = 1, nargs = '?',
                                help = 'number of masked scans')
equalisation_parser.add_argument('--velo', action = 'store_const', const = True, 
                                help = 'equalise each module in this directory tree')

# noise analysis parser
noise_parser = subparser.add_parser('noise', help = 'noise analysis of a quick/normal/control scan')
noise_parser.set_defaults(which = 'noise')
noise_parser.add_argument('analysis', action = 'store', choices = ['groups'], 
                          help = 'analyse & visualise odd/even column and 16th rows pixel noise')
noise_parser.add_argument('scan_type', action = 'store', choices = ['quick', 'normal', 'control'],
                          help = 'select the type of the scan to analyse')

# search parser
search_parser = subparser.add_parser('search', help = 'searching tool in calibration results')
search_parser.set_defaults(which = 'search')
search_parser.add_argument('key', action = 'store', metavar = None,
                          help = 'select the file tag (from nametmp) to look in')
search_parser.add_argument('criterion', action = 'store', metavar = None,
                          help = 'criterion for the lambda search (e.g. \"a<63\")')
search_parser.add_argument('velopix', action = 'store', metavar = 'VPx-y',
                          help = 'select the ASIC')

# plot parser
plot_parser = subparser.add_parser('plot', help = 'plots of all kind for calibration data')
plot_parser.set_defaults(which = 'plot')
plot_parser.add_argument('scan_type', action = 'store', choices = ['quick', 'normal', 'control'],
                                help = 'select the type of the scan to plot from')
plot_parser.add_argument('--velo', action = 'store_const', const = True, 
                        help = 'make plots for all modules')
plot_parser.add_argument('--all', '-a', action = 'store_const', const = True,
                         help = 'make all available plots for certain type of scan')
plot_parser.add_argument('-plot_mask', '-m', action = 'store_const', const = True, 
                         help = 'make a 2D map of masked pixels')
plot_parser.add_argument('-plot_trim', '-t', action = 'store_const', const = True, 
                         help = 'make a 2D map & a histogram of trim')
plot_parser.add_argument('-plot_normal', '-s', action = 'store_const', const = True,
                         help = 'make an equalisation summary plot')
plot_parser.add_argument('-plot_control', '-c', action = 'store_const', const = True,
                         help = 'make an equalisation control plot')
plot_parser.add_argument('-plot_dist', '-d', action = 'store_const', const = True,
                         help = 'compute the distance between rate 0x0 & 0xF')
plot_parser.add_argument('-plot_ns', '-ns', action = 'store_const', const = True,
                         help = 'make a 2D map & a histogram of noise sigma')
plot_parser.add_argument('-plot_nr', '-nr', action = 'store_const', const = True,
                         help = 'make a 2D map of noise rate')
plot_parser.add_argument('-plot_nm', '-nm', action = 'store_const', const = True,
                         help = 'make a 2D map of noise mean (baseline)')
plot_parser.add_argument('-plot_nc', '-nc', action = 'store_const', const = True,
                         help = 'scatter sigma against rate & base to visualise noise correlations')
plot_parser.add_argument('-plot_pix', '-p', action = 'store', nargs = 3, metavar = ('VP','X', 'Y'),
                         help = 'make an equalisation summary for a pixel in <VP> of coords <X> <Y>')

# noise analysis parser
monitoring_parser = subparser.add_parser('monitoring', help = 'monitoring, history')
monitoring_parser.set_defaults(which = 'monitoring')
monitoring_parser.add_argument('history', action = 'store', choices = ['nsplit'], 
                          help = 'dummy')
#monitoring_parser.add_argument('scan_type', action = 'store', choices = ['quick', 'normal', 'control'],
#                          help = 'dummy')

# status parser
status_parser = subparser.add_parser('status', help = 'pysoft status (developing version)')
status_parser.set_defaults(which = 'status')
status_parser.add_argument('status_of', action = 'store', choices = ['equalisation', 'summary', 'update','threshold', 'masked', 'spread', 'baseline', 'dacsettings'], 
                          help = '---')
status_parser.add_argument('--dir', '-d', action = 'store', required = True,
                         help = 'path to the equalisation directory')
status_parser.add_argument('--tag', action = 'store', default = '', required = False, help = 'tag the output plots with the recipe name')


#recipe parser
recipe_parser = subparser.add_parser('recipe', help = 'first recipe')
recipe_parser.set_defaults(which = 'recipe')
recipe_parser.add_argument('recipe_of', action = 'store', choices = ['threshold'], help =  '---')
recipe_parser.add_argument('--dir', '-d', action = 'store', required = False, default = '',
                         help = 'path to the equalisation directory')

#ivelo parser
ivelo_parser = subparser.add_parser('ivelo', help = 'open iVELO')
ivelo_parser.set_defaults(which = 'ivelo')
#gui_parser.add_argument('gui_of', action = 'store', choices = ['control'], help =  '---')
ivelo_parser.add_argument('--dir', '-d', action = 'store', required = False, default = '',
                         help = 'path to the <being tested>')