Usage
=====

Installation
------------

To set up pyVetra act as follows:

- Git clone pyVetra repository: ``ssh://git@gitlab.cern.ch:7999/ivelo/pyvetra.git``.
- Open your ``.bashrc`` file with an editor if your choice (*e.g.* ``vim ~/.bashrc``).
- Add the following line: ``alias pyvetra='~/<mypath>/myvetra/run.py'``.
- Source the *bashrc* file: ``source ~/.bashrc``.
